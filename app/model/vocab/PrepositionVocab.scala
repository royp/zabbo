package model.vocab

object PrepositionVocab extends Vocab {
  override def set = VocabResources.loadVocab("prepositions.txt").toVocab
}