package controllers

import model.insult.Insult
import play.api.mvc._

object Zabbo extends Controller {

  def respond(seed: String) = Action { request =>
    Ok(Insult.fetch)
  }

}
