package model.insult

abstract class ItemSet {
  def get: Seq[SingleItem]
}