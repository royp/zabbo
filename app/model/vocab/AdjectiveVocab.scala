package model.vocab

object AdjectiveVocab extends Vocab {
  override def set = VocabResources.loadVocab("adjectives.txt").toVocab
}

object AdjectiveGenericVocab extends Vocab {
  override def set = VocabResources.loadVocab("generic-adjectives.txt").toVocab
}