package model.vocab

import play.api.Logger

object VocabResources {

  def loadVocab(vocab: String): Seq[String] = {
    val wordstream = Option {
          getClass.getClassLoader.getResourceAsStream(vocab)
        } orElse {
          utils.BlindIO.readResource(vocab)
        } getOrElse {
          sys.error("Could not load vocab list, file not found")
        }
    try {
      io.Source.fromInputStream(wordstream).getLines().toList
    } catch {
      case e: Exception =>
        Logger.error(f"Could not load word list: $e")
        throw e
    } finally {
      wordstream.close()
    }
  }

}
