package model.contents

import model.insult._
import random._

abstract class Contents {

  val odds = set

  def set: Odds[Seq[ItemSet]]

  def get = odds.get

  def seq(items: ItemSet*) = items
}

object NounPhraseContents extends Contents {

  override def set = {
    OddBuilder
      .add(1, seq(Noun))
      .add(1, seq(AdjectivePhrase, Conjunction(" "), Noun))
      .add(1, seq(NounLeft, Conjunction(" "), Noun))
  }
}

object NounLeftContents extends Contents {

  override def set = {
    OddBuilder
      .add(2, seq(Noun))
      .add(1, seq(AdjectivePhrase, Conjunction(" "), Noun))
  }
}

object NounContents extends Contents {

  override def set = {
    OddBuilder
      .add(1, seq(NounThing))
      .add(1, seq(NounActivity))
      .add(2, seq(NounThing, NounActivityGeneric))
  }
}

object AdjectivePhraseContents extends Contents {

  override def set = {
    OddBuilder
      .add(3, seq(AdjectiveStar))
      .add(1, seq(AdverbStar, Conjunction(" "), AdjectiveStar))
      .add(2, seq(AdjectiveStar, Conjunction(" "), AdjectiveStar))
  }
}

object AdverbStarContents extends Contents {

  override def set = {
    OddBuilder
      .add(4, seq(Adverb))
      .add(1, seq(NounThing, AdverbGeneric))
  }
}

object AdjectiveStarContents extends Contents {

  override def set = {
    OddBuilder
      .add(2, seq(Adjective))
      .add(1, seq(NounThing, AdjectiveGeneric))
  }
}

object SentenceContents extends Contents {

  override def set = {
    OddBuilder
      .add(4, seq(Conjunction("you "), NounPhrase))
      .add(2, seq(Participle, Conjunction(" "), NounPhrase))
      .add(2, seq(Conjunction("you "), NounPhrase, Conjunction(" coupled with a "), NounPhrase))
      .add(1, seq(Participle, Conjunction(" "), NounPhrase, Conjunction(" coupled with a "),
                  NounPhrase))
      .add(2, seq(NounPhrase, Preposition))
  }
}