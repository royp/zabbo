package model.vocab

object AdverbVocab extends Vocab {
  override def set = VocabResources.loadVocab("adverbs.txt").toVocab
}

object AdverbGenericVocab extends Vocab {
    override def set = VocabResources.loadVocab("generic-adverb.txt").toVocab
}

