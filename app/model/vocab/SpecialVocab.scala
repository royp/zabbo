package model.vocab

object SpecialVocab extends Vocab {
  override def set = VocabResources.loadVocab("specials.txt").toVocab
}