package utils

import java.io.File

import com.typesafe.config.ConfigFactory

object BlindIO {

  val conf = ConfigFactory.load

  def subFile(base: File, children: Seq[String]) = {
    children.foldLeft(base)((file, child) => new File(file, child))
  }

  /** @return a resource from the `data/vocab` directory */
  def readResource(resourcePath: String): Option[java.io.InputStream] = {
    val classesDir = new File(getClass.getResource(".").toURI)
    val projectDir = classesDir.getParentFile.getParentFile.getParentFile.getParentFile
    val resourceFile = subFile(projectDir, conf.getString("vocab.file.path") :: List(resourcePath))
    if (resourceFile.exists) {
      Some(new java.io.FileInputStream(resourceFile))
    } else {
      None
    }
  }
}
