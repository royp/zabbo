package model.vocab

object NounGenericableVocab extends Vocab {
  override def set = VocabResources.loadVocab("genericable-nouns.txt").toVocab
}