package model.vocab

import utils.Random._

object NounVocab extends Vocab {
  override def set = VocabResources.loadVocab("nouns.txt").toVocab ++
    Seq(() => "[GENERIC INSULT #" + (rand nextInt 420 + 1) + "]")
}

object NounGenericVocab extends Vocab {
  override def set = VocabResources.loadVocab("generic-nouns.txt").toVocab
}