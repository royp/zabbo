package model.vocab

object ParticipleVocab extends Vocab {
  override def set = VocabResources.loadVocab("participle.txt").toVocab
}